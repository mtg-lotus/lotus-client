import React from 'react';

import type { ChildrenProps, RenderableWithChildren } from '@definitions/react';

export const reduceWrappers = (Comp: RenderableWithChildren, wrappers: RenderableWithChildren[] = []): RenderableWithChildren => {
  return wrappers.reduce<RenderableWithChildren>((Acc, Curr) => {
    return function Wrapper<T extends ChildrenProps> (props: T) {
      return (
        <Curr>
          <Acc {...props} />
        </Curr>
      );
    };
  }, Comp);
};
