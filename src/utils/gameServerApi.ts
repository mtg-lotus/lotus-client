import { Client, type RoomAvailable, type Room } from 'colyseus.js';
import { EventEmitter } from 'events';

const GAME_SERVER_URL = process.env.GAME_SERVER_URL ?? 'ws://localhost:2567';

export const client = new Client(GAME_SERVER_URL);

export class GlobalLobby extends EventEmitter {
  private static __singleton: GlobalLobby | null;

  private readonly __lobbyRoom: Room;
  private __rooms: RoomAvailable[] = [];

  public static async getLobby (): Promise<GlobalLobby> {
    if (!GlobalLobby.__singleton) {
      GlobalLobby.__singleton = new GlobalLobby(await client.joinOrCreate('global_lobby'));
    }

    return GlobalLobby.__singleton;
  }

  public get rooms (): RoomAvailable[] {
    return this.__rooms;
  }

  public get lobbyRoom (): Room {
    return this.__lobbyRoom;
  }

  constructor (lobbyRoom: Room) {
    super();
    this.__lobbyRoom = lobbyRoom;

    this.__lobbyRoom.onMessage('rooms', this.__onRooms.bind(this));
    this.__lobbyRoom.onMessage('+', this.__onRoomAdd.bind(this));
    this.__lobbyRoom.onMessage('-', this.__onRoomRemove.bind(this));
  }

  private __onRooms (rooms: RoomAvailable[]): void {
    this.__rooms = rooms;

    this.emit('rooms', this.__rooms);
  }

  private __onRoomAdd ([roomId, room]: [string, RoomAvailable]): void {
    const roomIndex = this.__rooms.findIndex((r) => r.roomId === roomId);

    if (roomIndex !== -1) {
      this.__rooms[roomIndex] = room;
    } else {
      this.__rooms.push(room);
    }

    this.emit('rooms', this.__rooms);
  }

  private __onRoomRemove (roomId: string): void {
    this.__rooms = this.__rooms.filter((r) => r.roomId !== roomId);

    this.emit('rooms', this.__rooms);
  }
}
