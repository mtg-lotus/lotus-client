import { gql } from '@apollo/client';

export const FULL_USER_FRAGMENT = gql`
  fragment FullUser on User {
    id
    username
    email
    firstName
    lastName
    image
    scopes
  }
`;