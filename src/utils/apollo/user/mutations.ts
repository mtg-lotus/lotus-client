import { gql, TypedDocumentNode } from '@apollo/client';
import { NexusGenArgTypes, NexusGenFieldTypes } from '@lotus/server/@types/nexus-typegen';

import { FULL_USER_FRAGMENT } from './fragments';

export const LOGIN_USER: TypedDocumentNode<{ loginUser: NexusGenFieldTypes['Mutation']['loginUser'] }, NexusGenArgTypes['Mutation']['loginUser']> = gql`
  ${FULL_USER_FRAGMENT}
  mutation LoginUserMutation($email: String!, $password: String!) {
    loginUser(email: $email, password: $password) {
      refreshToken
      user {
        ...FullUser
      }
    }
  }
`;

export const CREATE_USER: TypedDocumentNode<{ createUser: NexusGenFieldTypes['Mutation']['createUser'] }, NexusGenArgTypes['Mutation']['createUser']> = gql`
  ${FULL_USER_FRAGMENT}
  mutation CreateUserMutation($email: String!, $username: String!, $firstName: String!, $lastName: String!, $password: String!, $scopes: [Role!]) {
    createUser(email: $email, username: $username, firstName: $firstName, lastName: $lastName, password: $password, scopes: $scopes) {
      ...FullUser
    }
  }
`;

export const REFRESH_USER: TypedDocumentNode<{ refresh: NexusGenFieldTypes['Mutation']['refresh'] }, NexusGenArgTypes['Mutation']['refresh']> = gql`
  ${FULL_USER_FRAGMENT}
  mutation RefreshUser($refreshToken: String!) {
    refresh(refreshToken: $refreshToken) {
      ...FullUser
    }
  }
`;