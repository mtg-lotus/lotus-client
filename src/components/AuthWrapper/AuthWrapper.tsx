import * as React from 'react';

import useAuth from '@context/auth';
import { Navigate } from 'react-router-dom';

export const AuthWrapper: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const { isAuthenticated, loading } = useAuth();

  if (loading) {
    return <div>Loading ...</div>;
  }

  if (isAuthenticated) {
    return (
      <>
        {children}
      </>
    );
  }

  return <Navigate to="/auth/login" />;
};

export default AuthWrapper;
