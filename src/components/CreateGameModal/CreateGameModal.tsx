import * as React from 'react';

import {
  Modal,
  Form,
  Input,
  Button,
  Select,
  Checkbox,
  InputNumber,
  Space,
} from 'antd';

import { type GameCreationOptions } from '@definitions/gameServer';

export interface CreateGameModalProps {
  onConfirm: (lobbyName: string, options: GameCreationOptions) => void | Promise<void>
  onCancel: () => void
  open: boolean
}

export const CreateGameModal: React.FC<CreateGameModalProps> = ({ onConfirm, onCancel, open }) => {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [form] = Form.useForm();

  const handleConfirm = async ({
    name,
    description,
    maxPlayers,
    format,
    privateRoom,
    password,
  }: {
    name: string
    description: string
    maxPlayers?: number
    format: string
    privateRoom: boolean
    password?: string
  }): Promise<void> => {
    setLoading(true);

    await onConfirm(format, {
      name,
      description,
      maxPlayers,
      private: privateRoom,
      password,
    });

    setLoading(false);
  };

  const handleCancel = (): void => {
    onCancel();
  };

  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      title="Create Game"
      footer={[
        <Button
          key="cancel"
          onClick={handleCancel}
        >
          Cancel
        </Button>,
        <Button
          key="create"
          htmlType="submit"
          form="create-game-form"
          type="primary"
          loading={loading}
        >
          Create
        </Button>,
      ]}
    >
      <Form
        id="create-game-form"
        form={form}
        onFinish={(...args) => { void handleConfirm(...args); }}
      >
        <Form.Item
          name="name"
          label="Name"
          rules={[{ required: true, message: 'Name is required' }]}
        >
          <Input placeholder="Name" />
        </Form.Item>
        <Form.Item
          name="description"
          label="Description"
          rules={[{ required: true, message: 'Description is required' }]}
        >
          <Input.TextArea placeholder="Description" />
        </Form.Item>
        <Space direction="horizontal" align="start">
          <Form.Item
            name="format"
            label="Format"
            initialValue="commander_game_room"
            rules={[{ required: true, message: 'Game format is required' }]}
          >
            <Select
              onChange={(val) => {
                form.setFieldsValue({ format: val });
              }}
            >
              <Select.Option value="commander_game_room">Commander</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="maxPlayers"
            label="Max Players"
          >
            <InputNumber />
          </Form.Item>
          <Form.Item
            label="Visibility"
            name="privateRoom"
            valuePropName="checked"
            initialValue={false}
          >
            <Checkbox>Private</Checkbox>
          </Form.Item>
        </Space>
        <Form.Item
          name="password"
          label="Password"
        >
          <Input.Password placeholder="Password" />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default CreateGameModal;
