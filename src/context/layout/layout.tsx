import { type FCWithChildren } from '@definitions/react';
import * as React from 'react';

export interface LayoutContextType {
  title: string
  setTitle: (val: string) => void
}

const DEFAULT_VALUE: LayoutContextType = {
  title: '',
  setTitle: () => {},
};

const LayoutContext = React.createContext(DEFAULT_VALUE);

export const LayoutProvider: FCWithChildren = ({ children }) => {
  const [title, setTitle] = React.useState<string>('');

  const value = React.useMemo(() => ({
    title,
    setTitle,
  }), [
    title,
    setTitle,
  ]);

  return (
    <LayoutContext.Provider value={value}>
      {children}
    </LayoutContext.Provider>
  );
};

export const useLayout = (): LayoutContextType =>
  React.useContext(LayoutContext);

export default useLayout;
