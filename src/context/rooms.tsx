import React from 'react';

import type { Room, RoomAvailable } from 'colyseus.js';

import { client, GlobalLobby } from '@utils/gameServerApi';
export interface RoomsContextType {
  rooms: RoomAvailable[]
  error?: Error
  loading: boolean
  activeRoom: string | null
  setActiveRoom: (val: string | null) => Promise<void>
}

const DEFAULT_VALUE: RoomsContextType = {
  rooms: [],
  loading: false,
  activeRoom: null,
  setActiveRoom: async () => {},
};

const RoomsContext = React.createContext<RoomsContextType>(DEFAULT_VALUE);

interface RoomsProviderProps {
  children: React.ReactNode
}

interface RoomsProviderState {
  rooms: RoomAvailable[]
  loading: boolean
  lobby: GlobalLobby | null
  error?: Error
  activeRoom: Room | null
}

export class RoomsProvider extends React.Component<RoomsProviderProps> {
  public readonly state: RoomsProviderState = {
    rooms: [],
    loading: false,
    lobby: null,
    activeRoom: null,
  };

  public componentDidMount (): void {
    void this.__loadLobby();
  }

  private async __loadLobby (): Promise<void> {
    this.setState({
      ...this.state,
      loading: true,
    });

    const lobby = await GlobalLobby.getLobby();
    lobby.on('rooms', this.__setRooms.bind(this));

    this.setState({
      ...this.state,
      loading: false,
    });
  }

  private __setRooms (rooms: Room[]): void {
    this.setState({
      ...this.state,
      rooms,
    });
  }

  private async __setActiveRoom (roomId: string | null): Promise<void> {
    const { activeRoom } = this.state;

    if (activeRoom) {
      await activeRoom.leave();
    }

    if (roomId === null) {
      this.setState({
        ...this.state,
        activeRoom: null,
      });
      return;
    }

    let room: Room | null = null;

    try {
      room = await client.joinById(roomId);
    } catch (e) {
    } finally {
      this.setState({
        ...this.state,
        activeRoom: room,
      });
    }
  }

  public render (): React.ReactElement<any, any> {
    return (
      <RoomsContext.Provider
        value={{
          rooms: this.state.rooms,
          loading: false,
          activeRoom: this.state.activeRoom?.id ?? null,
          setActiveRoom: this.__setActiveRoom.bind(this),
        }}
      >
        {this.props.children}
      </RoomsContext.Provider>
    );
  };
}

export const useRooms = (): RoomsContextType =>
  React.useContext(RoomsContext);

export default useRooms;
