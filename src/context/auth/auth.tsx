import React from 'react';

import { useMutation } from '@apollo/client';
import {
  REFRESH_USER,
  LOGIN_USER,
} from '@utils/apollo/user';
import { NexusGenRootTypes } from '@lotus/server/@types/nexus-typegen';
import { useNavigate } from 'react-router-dom';

export interface AuthContextType {
  user: NexusGenRootTypes['User'] | null
  loading: boolean
  isAuthenticated: boolean
  error: any
  login: (email: string, password: string) => Promise<void>
  logout: () => Promise<void>
}

const DEFAULT_VALUE: AuthContextType = {
  user: null,
  loading: false,
  isAuthenticated: false,
  login: async () => { },
  logout: async () => { },
  error: null,
};

const AuthContext = React.createContext<AuthContextType>(DEFAULT_VALUE);

export const AuthProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [user, setUser] = React.useState<AuthContextType['user']>(null);
  const [error, setError] = React.useState<any>();
  const navigate = useNavigate();

  const [loginMutation, { loading: loginLoading }] = useMutation(LOGIN_USER);
  const [refreshMutation, { loading: refreshLoading }] = useMutation(REFRESH_USER);

  React.useEffect(() => {
    void (async () => {
      const refreshToken = await window.electronStore.getItem('tokens.refreshToken');

      if (refreshToken) {
        const res = await refreshMutation({ variables: { refreshToken }});
        if (res.data?.refresh) {
          setUser(res.data.refresh);
          navigate('/app');
        }
      }
    })().catch((err) => {
      setError(err);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const login = React.useCallback(async (email: string, password: string) => {
    try {
      const res = await loginMutation({
        variables: {
          email,
          password,
        }
      });
  
      if (res.data?.loginUser) {
        window.electronStore.setItem('tokens.refreshToken', res.data.loginUser.refreshToken);
        setUser(res.data.loginUser.user);
      }
    } catch (e) {
      setError(e);
    }
  }, [loginMutation]);

  const logout = React.useCallback(async () => {
    window.electronStore.deleteItem('tokens.refreshToken');
    setUser(null);
  }, []);

  const value = React.useMemo(() => ({
    loading: loginLoading || refreshLoading,
    login,
    logout,
    user,
    error,
    isAuthenticated: Boolean(user),
  } as AuthContextType), [
    loginLoading,
    refreshLoading,
    login,
    logout,
    user,
    error,
  ]);

  return (
    <AuthContext.Provider value={value}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = (): AuthContextType =>
  React.useContext(AuthContext);

export default useAuth;
