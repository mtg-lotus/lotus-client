import {
  type AuthContextType,
  AuthProvider,
  useAuth,
} from './auth';

export {
  type AuthContextType,
  AuthProvider,
  useAuth,
};

export default useAuth;
