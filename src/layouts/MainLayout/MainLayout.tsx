import * as React from 'react';

import {
  Outlet,
  useNavigate,
} from 'react-router-dom';

import { Breadcrumb, Button, Layout, Menu, type MenuItemProps } from 'antd';
import { LockOutlined, GlobalOutlined, PlusOutlined } from '@ant-design/icons';

import styled from 'styled-components';

import useAuth from '@context/auth';
import useRooms from '@context/rooms';
import CreateGameModal from '@components/CreateGameModal';
import { type GameCreationOptions } from '@definitions/gameServer';
import { client } from '@utils/gameServerApi';

const { Header, Content, Sider } = Layout;

// const Root = styled.div`
//   height: 100%;
//   width: 100%;
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   background-color: #E0E0E0;
// `;
const Logo = styled.div`
  float: left;
  width: 120px;
  height: 31px;
  margin: 16px 24px 16px 0;
  background: rgba(255, 255, 255, 0.3);
`;

const InnerContent = styled.div`
  width: 100%;
  height: 100%;
  padding: 16px;
`;

export const MainLayout: React.FC = () => {
  const { user } = useAuth();
  const { rooms, setActiveRoom } = useRooms();
  const [createOpen, setCreateOpen] = React.useState<boolean>(false);

  const navigate = useNavigate();

  const handleCreationCancel = (): void => {
    setCreateOpen(false);
  };

  const handleCreation = async (name: string, options: GameCreationOptions): Promise<void> => {
    console.log('CREATE', name, options);
    const token = await window.electronStore.getItem('tokens.accessToken');

    await client.create(name, {
      ...options,
      accessToken: token
    });
    setCreateOpen(false);
  };

  const handleNavMenuClick: MenuItemProps['onClick'] = (e) => {
    React.startTransition(() => {
      navigate(e.key);
    });
  };
  return (
    <Layout style={{ width: '100%', height: '100%' }}>
      <Header className="header" style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
        <Logo />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={[]}
          items={[
            {
              label: 'Home',
              key: '/app',
              onClick: handleNavMenuClick,
            },
            {
              label: 'Decks',
              key: '/app/decks',
              onClick: handleNavMenuClick,
            },
            {
              label: 'Groups',
              key: '/app/groups',
              onClick: handleNavMenuClick,
            },
          ]}
          style={{ flexGrow: 1 }}
        />
        <Button shape="circle">
          {user?.image ?? `${user?.firstName?.charAt(0).toUpperCase() as string}${user?.lastName?.charAt(0).toUpperCase() as string}`}
        </Button>
      </Header>
      <Content style={{ padding: '0 48px' }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
        </Breadcrumb>
        <Layout style={{ background: 'white', padding: 16, height: 'calc(100% - 48px - 16px)', overflowY: 'auto' }}>
          <Sider style={{ background: 'white' }} width={300}>
            <Button style={{ width: '100%' }} icon={<PlusOutlined />} onClick={() => { setCreateOpen(true); }}>
              New Game
            </Button>
            <Menu
              mode="vertical"
              defaultSelectedKeys={[]}
              defaultOpenKeys={[]}
              style={{ padding: 0, margin: 0 }}
              items={rooms.map((room) => ({
                label: room.metadata.name,
                key: room.roomId,
                disabled: room.metadata.private,
                icon: room.metadata.private ? <LockOutlined /> : <GlobalOutlined />,
                onClick: () => { void setActiveRoom(room.roomId); },
              }))}
            />
          </Sider>
          <InnerContent>
            <Outlet />
          </InnerContent>
        </Layout>
      </Content>
      <CreateGameModal open={createOpen} onConfirm={handleCreation} onCancel={handleCreationCancel} />
    </Layout>
  );
};

export default MainLayout;
