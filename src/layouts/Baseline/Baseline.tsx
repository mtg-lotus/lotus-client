import * as React from 'react';
import { Outlet, useLocation } from 'react-router-dom';

import styled from 'styled-components';

import { LayoutProvider } from '@context/layout';
import { AuthProvider } from '@context/auth';

const Root = styled.div`
  display: flex;
  position: fixed;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #E0E0E0;
`;

export const Baseline: React.FC = () => {
  const { pathname } = useLocation();
  console.log('PATH', pathname);

  return (
    <Root>
      <LayoutProvider>
        <AuthProvider>
          <React.Suspense fallback={<div>Loading ...</div>}>
            <Outlet />
          </React.Suspense>
        </AuthProvider>
      </LayoutProvider>
    </Root>
  );
};

export default Baseline;
