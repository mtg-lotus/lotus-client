import * as React from 'react';
import { Outlet } from 'react-router-dom';

import { Card } from 'antd';

import useLayout from '@context/layout';

export const AuthLayout: React.FC = () => {
  const { title } = useLayout();

  return (
    <Card title={title}>
      <Outlet />
    </Card>
  );
};

export default AuthLayout;
