export interface GameCreationOptions {
  name: string
  description: string
  private?: boolean
  password?: string
  maxPlayers?: number
}
