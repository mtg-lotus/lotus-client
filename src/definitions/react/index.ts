import type {
  JSXElementConstructor,
  ReactNode,
  Component,
  FC,
} from 'react';

export interface ChildrenProps {
  children: ReactNode
}

export type RenderableWithChildren = JSXElementConstructor<ChildrenProps>;
export type FCWithChildren = FC<ChildrenProps>;
export type ComponentWithChildren = Component<ChildrenProps>;

export type FCWithWrappers<T = object> = FC<T> & {
  wrappers: RenderableWithChildren[]
};
