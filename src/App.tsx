import React from 'react';
import '@styles/App.css';

import { ApolloProvider } from '@apollo/client';
import { RouterProvider } from 'react-router-dom';

import { client } from '@utils/apollo';
import router from './routes';

const App: React.FC = () => {
  return (
    <ApolloProvider client={client}>
      <RouterProvider router={router} />
    </ApolloProvider>
  );
};

export default App;
