import * as React from 'react';

import { createHashRouter, Navigate } from 'react-router-dom';

import AuthLayout from '@layouts/AuthLayout';
import Baseline from '@layouts/Baseline';
import MainLayout from '@layouts/MainLayout/MainLayout';

import appRoutes from './app';
import authRoutes from './auth';

import AuthWrapper from '@components/AuthWrapper';
import { RoomsProvider } from '@context/rooms';

export const router = createHashRouter([
  {
    element: <Baseline />,
    children: [
      {
        path: '/auth',
        element: <AuthLayout />,
        children: [
          ...authRoutes,
        ],
      },
      {
        path: '/app',
        element: (
          <AuthWrapper>
            <RoomsProvider>
              <MainLayout />
            </RoomsProvider>
          </AuthWrapper>
        ),
        children: [
          ...appRoutes,
        ],
      },
      {
        index: true,
        element: <Navigate to="/auth/login" />,
      },
    ],
  },
]);

export default router;
