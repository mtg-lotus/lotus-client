import * as React from 'react';

import { type RouteObject } from 'react-router-dom';

const Decks = React.lazy(async () => await import('./Decks'));
const Groups = React.lazy(async () => await import('./Groups'));
const Home = React.lazy(async () => await import('./Home'));

export const routes: RouteObject[] = [
  {
    index: true,
    element: <Home />,
  },
  {
    path: 'decks',
    element: <Decks />,
  },
  {
    path: 'groups',
    element: <Groups />,
  },
];

export default routes;
