import React from 'react';

import styled from 'styled-components';

import useUser from '@context/auth/auth';

const Root = styled.div`

`;

export const HomePage: React.FC = () => {
  const { loading } = useUser();

  React.useEffect(() => {
    window.electronApi.on('nothing-to-download', () => {
      alert('Nothing to download');
    });
  }, []);

  return (
    <Root>
      {loading
        ? (
          <div>Loading ...</div>
        )
        : (
          <div>
            Home
          </div>
        )
      }
    </Root>
  );
};

export default HomePage;
