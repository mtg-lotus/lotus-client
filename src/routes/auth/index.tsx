import * as React from 'react';

import { type RouteObject } from 'react-router-dom';

const Login = React.lazy(async () => await import('./Login'));
const Signup = React.lazy(async () => await import('./Signup'));

export const routes: RouteObject[] = [
  {
    path: 'login',
    element: <Login />,
  },
  {
    path: 'signup',
    element: <Signup />,
  },
];

export default routes;
