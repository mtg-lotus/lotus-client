import React, { useState } from 'react';

import {
  Button,
  Input,
  Form,
  type FormProps,
  message,
} from 'antd';

import { CREATE_USER } from '@utils/apollo/user';
import useAuth from '@context/auth';

import { Link, useNavigate } from 'react-router-dom';
import useLayout from '@context/layout';
import { useMutation } from '@apollo/client';

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

export const SignupPage: React.FC = () => {
  const [passwordError, setPasswordError] = useState(false);
  const [loading, setLoading] = useState(false);
  const { login } = useAuth();
  const navigate = useNavigate();
  const { setTitle } = useLayout();
  const [createUser] = useMutation(CREATE_USER);

  React.useEffect(() => {
    setTitle('Sign up for Lotus');
  }, [setTitle]);

  const handleSignupSubmit = async ({
    firstName,
    lastName,
    email,
    username,
    password,
    passwordConfirm,
  }: {
    firstName: string
    lastName: string
    email: string
    username: string
    password: string
    passwordConfirm: string
  }): Promise<void> => {
    if (password !== passwordConfirm) {
      setPasswordError(true);
      return;
    }
    setPasswordError(false);

    setLoading(true);

    try {
      await createUser({
        variables: {
          email,
          username,
          firstName,
          lastName,
          password,
          scopes: ['USER']
        }
      });

      await login(email, password);

      navigate('/app/');
    } catch (e) {
      await message.open({
        type: 'error',
        content: 'Failed to create user',
      });
    } finally {
      setLoading(false);
    }
  };

  const handleFail: FormProps['onFinishFailed'] = (errInfo) => {
    console.error(errInfo.errorFields);
  };

  return (
    <>
      <Form
        name="signup"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={(...args) => { void handleSignupSubmit(...args); }}
        onFinishFailed={handleFail}
        autoComplete="off"
        labelWrap
        layout="horizontal"
      >
        <Form.Item
          name="firstName"
          label="First Name"
          rules={[{ required: true, message: 'Please input your first name.' }]}
        >
          <Input
            placeholder="First Name"
          />
        </Form.Item>
        <Form.Item
          name="lastName"
          label="Last Name"
          rules={[{ required: true, message: 'Please input your last name.' }]}
        >
          <Input
            placeholder="Last Name"
          />
        </Form.Item>
        <Form.Item
          name="email"
          label="Email"
          rules={[{ required: true, message: 'Please input your email.' }]}
        >
          <Input
            placeholder="Email"
          />
        </Form.Item>
        <Form.Item
          name="username"
          label="Username"
          rules={[{ required: true, message: 'Please input your username.' }]}
        >
          <Input
            placeholder="Username"
          />
        </Form.Item>
        <Form.Item
          name="password"
          label="Password"
          rules={[{ required: true, message: 'Please input your password.' }]}
        >
          <Input.Password
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item
          name="passwordConfirm"
          label="Confirm Password"
          validateStatus={passwordError ? 'error' : undefined}
          help={passwordError ? 'Passwords did not match' : undefined}
          rules={[{ required: true, message: 'Please confirm your password.' }]}
        >
          <Input.Password
            placeholder="Confirm Password"
          />
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button
            type="primary"
            htmlType="submit"
            loading={loading}
            style={{ marginRight: 16 }}
          >
                Sign Up
          </Button>
          <Link to="/auth/login">Sign In</Link>
        </Form.Item>
      </Form>
    </>
  );
};

export default SignupPage;
