import React, { useState } from 'react';

import {
  Button,
  Input,
  Form,
  type FormProps,
  message,
} from 'antd';

import { Link, useNavigate } from 'react-router-dom';
import useAuth from '@context/auth';
import useLayout from '@context/layout';

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

export const LoginPage: React.FC = () => {
  const { login } = useAuth();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const { setTitle } = useLayout();

  React.useEffect(() => {
    setTitle('Log in to Lotus');
  }, []);

  const handleLoginSubmit = async ({ email, password }: { email: string, password: string }): Promise<void> => {
    setLoading(true);
    try {
      await login(email, password);

      navigate('/app/');
    } catch (e) {
      await message.open({
        type: 'error',
        content: 'Login failed',
      });
    } finally {
      setLoading(false);
    }
  };

  const handleFail: FormProps['onFinishFailed'] = (errInfo) => {
    console.error(errInfo.errorFields);
  };

  return (
    <>
      <Form
        name="signup"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={(...args) => { void handleLoginSubmit(...args); }}
        onFinishFailed={handleFail}
        autoComplete="off"
        labelWrap
        layout="horizontal"
      >
        <Form.Item
          name="email"
          label="Email"
          rules={[{ required: true, message: 'Please input your email.' }]}
        >
          <Input
            placeholder="Email"
          />
        </Form.Item>
        <Form.Item
          name="password"
          label="Password"
          rules={[{ required: true, message: 'Please input your password.' }]}
        >
          <Input.Password
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button
            type="primary"
            htmlType="submit"
            loading={loading}
            style={{ marginRight: 16 }}
          >
                Sign In
          </Button>
          <Link to="/auth/signup">Sign Up</Link>
        </Form.Item>
      </Form>
    </>
  );
};

export default LoginPage;
