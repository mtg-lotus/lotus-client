import ObjFile from 'obj-file-parser';

import {
  createArrayBuffer,
  createElementArrayBuffer,
  enableElementArrayBuffer,
  enableVec2Float32Buffer,
  enableVec3Float32Buffer,
} from '../utils/buffers';

export class Mesh {
  private readonly __context: WebGLRenderingContext;

  private readonly __vertices: number[];
  private readonly __indices: number[];
  private readonly __uvs: number[];
  private readonly __normals: number[];

  private __vertexBuffer: WebGLBuffer | null = null;
  private __indexBuffer: WebGLBuffer | null = null;
  private __uvBuffer: WebGLBuffer | null = null;
  private __normalBuffer: WebGLBuffer | null = null;

  private static readonly __meshMap: Record<string, Mesh> = {};

  public static async fromObjFileObject (ctx: WebGLRenderingContext, fileName: string): Promise<Mesh[]> {
    const res = await fetch(fileName);
    const text = await res.text();

    const objFile = new ObjFile(text);
    const output = objFile.parse();

    return output.models.map((model) => {
      if (this.__meshMap[model.name]) {
        return this.__meshMap[model.name];
      }

      const { faces, textureCoords, vertexNormals, vertices: objVertices } = model;

      let vertices: number[] = [];
      let normals: number[] = [];
      let uvs: number[] = [];

      const indices: number[] = [];

      for (let i = 0; i < faces.length * 3; i++) {
        const face = faces[Math.floor(i / 3)];
        const vertex = face.vertices[i % 3];

        indices.push(i);
        vertices = [
          ...vertices,
          objVertices[vertex.vertexIndex - 1].x,
          objVertices[vertex.vertexIndex - 1].y,
          objVertices[vertex.vertexIndex - 1].z,
        ];

        normals = [
          ...normals,
          vertexNormals[vertex.vertexNormalIndex - 1].x,
          vertexNormals[vertex.vertexNormalIndex - 1].y,
          vertexNormals[vertex.vertexNormalIndex - 1].z,
        ];

        uvs = [
          ...uvs,
          textureCoords[vertex.textureCoordsIndex - 1].u,
          textureCoords[vertex.textureCoordsIndex - 1].v,
        ];
      }

      const mesh = new Mesh(ctx, vertices, indices, uvs, normals);

      this.__meshMap[model.name] = mesh;
      return mesh;
    });
  }

  constructor (ctx: WebGLRenderingContext, vertices: number[], indices: number[], uvs: number[], normals: number[]) {
    this.__context = ctx;

    this.__vertices = vertices;
    this.__indices = indices;
    this.__uvs = uvs;
    this.__normals = normals;
  }

  public init (): void {
    this.__vertexBuffer = createArrayBuffer(this.__context, this.__vertices);
    this.__uvBuffer = createArrayBuffer(this.__context, this.__uvs);
    this.__normalBuffer = createArrayBuffer(this.__context, this.__normals);

    this.__indexBuffer = createElementArrayBuffer(this.__context, this.__indices);
  }

  public render (
    locations: { vertex: number, uv?: number, normal?: number },
  ): void {
    if (!this.__indexBuffer || !this.__vertexBuffer) {
      throw new Error('Cannot render without the appropriate buffers');
    }
    enableElementArrayBuffer(this.__context, this.__indexBuffer);
    enableVec3Float32Buffer(this.__context, locations.vertex, this.__vertexBuffer);

    if (locations.uv && this.__uvBuffer) {
      enableVec2Float32Buffer(this.__context, locations.uv, this.__uvBuffer);
    }
    if (locations.normal && this.__normalBuffer) {
      enableVec3Float32Buffer(this.__context, locations.normal, this.__normalBuffer);
    }

    this.__context.drawElements(this.__context.TRIANGLES, this.__indices.length, this.__context.UNSIGNED_SHORT, 0);
  }
}
