import ReactDOM from 'react-dom';
import React from 'react';

export const Portal: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [container, setContainer] = React.useState<HTMLDivElement | null>(null);
  const [extWindow, setExtWindow] = React.useState<Window | null>(null);

  React.useEffect(() => {
    const cont = document.createElement('div');
    setContainer(cont);

    const win = window.open('', '', 'width=640,height=480,left=200,top=200');
    setExtWindow(win);

    const style = document.createElement('style');
    style.innerHTML = `
      html, body {
        margin: 0;
        padding: 0;
        overflow: hidden;
      }
      div, html, body, canvas {
        width: 100%;
        height: 100%;
        display: block;
      }
    `;
    win?.document.body.appendChild(cont);
    win?.document.head.appendChild(style);

    return () => {
      win?.close();
    };
  }, []);

  if (!extWindow || !container) {
    return null;
  }

  return ReactDOM.createPortal(children, container);
};
