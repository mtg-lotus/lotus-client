export class Texture {
  private readonly __context: WebGLRenderingContext;
  private readonly __imagePath: string;
  private __texture: WebGLTexture | null = null;

  public get texture (): WebGLTexture | null {
    return this.__texture;
  }

  constructor (gl: WebGLRenderingContext, imagePath: string) {
    this.__context = gl;
    this.__imagePath = imagePath;
  }

  public init (): void {
    const texture = this.__context.createTexture();

    if (texture === null) {
      alert(`Failed to create texture ${this.__imagePath}`);
      return;
    }

    this.__context.bindTexture(this.__context.TEXTURE_2D, texture);

    this.__context.texImage2D(
      this.__context.TEXTURE_2D,
      0,
      this.__context.RGBA,
      1,
      1,
      0,
      this.__context.RGBA,
      this.__context.UNSIGNED_BYTE,
      new Uint8Array([0, 0, 255, 255]),
    );

    const image = new Image();

    image.onload = () => {
      this.__context.bindTexture(this.__context.TEXTURE_2D, texture);
      this.__context.texImage2D(
        this.__context.TEXTURE_2D,
        0,
        this.__context.RGBA,
        this.__context.RGBA,
        this.__context.UNSIGNED_BYTE,
        image,
      );

      if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
        this.__context.generateMipmap(this.__context.TEXTURE_2D);
      } else {
        this.__context.texParameteri(this.__context.TEXTURE_2D, this.__context.TEXTURE_WRAP_S, this.__context.CLAMP_TO_EDGE);
        this.__context.texParameteri(this.__context.TEXTURE_2D, this.__context.TEXTURE_WRAP_T, this.__context.CLAMP_TO_EDGE);
        this.__context.texParameteri(this.__context.TEXTURE_2D, this.__context.TEXTURE_MIN_FILTER, this.__context.LINEAR);
      }

      this.__context.pixelStorei(this.__context.UNPACK_FLIP_Y_WEBGL, true);
    };

    image.src = `image://${this.__imagePath}`;

    this.__texture = texture;
  }
}

const isPowerOf2 = (val: number): boolean => {
  return (val & (val - 1)) === 0;
};
