import { mat4 } from 'gl-matrix';
import { type Model } from '../models';

export interface RenderArgs {
  projMat: mat4
  modelViewMat: mat4
}

export class Render {
  private readonly __context: WebGLRenderingContext;

  constructor (gl: WebGLRenderingContext) {
    this.__context = gl;
  }

  public init (): void {
    this.__context.clearColor(0.0, 0.0, 0.0, 1.0);
    this.__context.clear(this.__context.COLOR_BUFFER_BIT);
  }

  public draw (models: Model[], delta: number): void {
    this.__context.clearColor(0.0, 0.0, 0.0, 1.0);
    this.__context.clearDepth(1.0);
    this.__context.enable(this.__context.DEPTH_TEST);
    this.__context.depthFunc(this.__context.LEQUAL);

    this.__context.clear(this.__context.COLOR_BUFFER_BIT | this.__context.DEPTH_BUFFER_BIT);

    const fov = (45 * Math.PI) / 180;
    const aspect = this.__context.canvas.width / this.__context.canvas.height;
    const zNear = 0.1;
    const zFar = 100.0;
    const projMat = mat4.create();

    mat4.perspective(projMat, fov, aspect, zNear, zFar);

    const modelViewMat = mat4.create();

    mat4.translate(modelViewMat, modelViewMat, [0, 0, -10.0]);

    models.forEach((model) => {
      model.render({ projMat, modelViewMat }, delta);
    });
  }
}
