import * as React from 'react';

import { Engine } from './engine/engine';

export const WebGLContext: React.FC = () => {
  const ref = React.createRef<HTMLCanvasElement>();

  React.useEffect(() => {
    if (ref.current) {
      const gl = ref.current?.getContext('webgl');

      if (gl === null) {
        alert('Cannot initialize webgl');
      } else {
        const engine = new Engine(gl);

        ref.current.ownerDocument.defaultView?.addEventListener('resize', () => {
          if (!ref.current) {
            return;
          }

          const displayWidth = ref.current.clientWidth;
          const displayHeight = ref.current.clientHeight;

          ref.current.width = displayWidth;
          ref.current.height = displayHeight;

          gl.viewport(0, 0, displayWidth, displayHeight);
        });

        void engine.init().then(() => {
          requestAnimationFrame(engine.render.bind(engine));
        });
      }
    }
  }, [ref.current]);

  return (
    <canvas ref={ref} />
  );
};
