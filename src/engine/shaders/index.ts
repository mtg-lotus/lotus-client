import * as simple from './simple';

export const shaders = {
  simple,
};

export class Shader<T extends keyof typeof shaders> {
  private readonly __context: WebGLRenderingContext;
  private readonly __shaderName: T;

  private readonly __program: WebGLProgram | null = null;

  private static readonly __compiledPrograms: Record<string, WebGLProgram> = {};

  public get program (): WebGLProgram | null {
    return this.__program;
  }

  constructor (gl: WebGLRenderingContext, key: T) {
    this.__shaderName = key;
    this.__context = gl;

    const prog = this.__loadProgram(this.__context, this.__shaderName);
    if (prog !== null) {
      this.__program = prog;
    }
  }

  public getInfo (): ReturnType<(typeof shaders[T])['getInfo']> {
    // @ts-expect-error idk
    return shaders[this.__shaderName].getInfo(this.__context, this.__program);
  }

  public link (): boolean {
    if (this.__program === null) {
      alert('trying to init null program');
      return false;
    }

    this.__context.linkProgram(this.__program);

    if (!this.__context.getProgramParameter(this.__program, this.__context.LINK_STATUS)) {
      alert(
        `Unable to initialize the shader program: ${this.__context.getProgramInfoLog(
          this.__program,
        ) as string}`,
      );

      return false;
    }

    return true;
  }

  private __loadProgram (gl: WebGLRenderingContext, key: T): WebGLProgram | null {
    if (Shader.__compiledPrograms[key]) {
      return Shader.__compiledPrograms[key];
    }

    const shaderSources = shaders[key];

    const vertShader = Shader.__compileShader(gl, gl.VERTEX_SHADER, shaderSources.vert);
    const fragShader = Shader.__compileShader(gl, gl.FRAGMENT_SHADER, shaderSources.frag);

    if (vertShader === null || fragShader === null) {
      alert(`an error occured creating shader program ${key as string}`);
      return null;
    }

    const program = gl.createProgram();

    if (program === null) {
      alert(`an error occured on program creation for ${key as string}`);
      return null;
    }

    gl.attachShader(program, vertShader);
    gl.attachShader(program, fragShader);

    Shader.__compiledPrograms[key] = program;
    return program;
  }

  private static __compileShader (gl: WebGLRenderingContext, type: number, source: string): WebGLShader | null {
    const shader = gl.createShader(type);

    if (shader === null) {
      alert('An error occured creating shader');
      return null;
    }

    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert(
        `An error occurred compiling the shaders: ${gl.getShaderInfoLog(shader) as string}`,
      );
      gl.deleteShader(shader);
      return null;
    }

    return shader;
  }
}
