export interface SimpleShaderInfo {
  attribLocations: {
    vertexPosition: number
    textureCoord: number
  }
  uniformLocations: {
    projMat: WebGLUniformLocation | null
    modelViewMat: WebGLUniformLocation | null
    uSampler: WebGLUniformLocation | null
  }
}

export const vert = `
  attribute vec4 aVertexPosition;
  attribute vec2 aTextureCoord;

  uniform mat4 uModelViewMatrix;
  uniform mat4 uProjectionMatrix;

  varying highp vec2 vTextureCoord;

  void main() {
    gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    vTextureCoord = aTextureCoord;
  }
`;

export const frag = `
  varying highp vec2 vTextureCoord;

  uniform sampler2D uSampler;

  void main() {
    gl_FragColor = texture2D(uSampler, vTextureCoord);
  }
`;

export const getInfo = (gl: WebGLRenderingContext, program: WebGLProgram): SimpleShaderInfo => {
  return {
    attribLocations: {
      vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
      textureCoord: gl.getAttribLocation(program, 'aTextureCoord'),
    },
    uniformLocations: {
      projMat: gl.getUniformLocation(program, 'uProjectionMatrix'),
      modelViewMat: gl.getUniformLocation(program, 'uModelViewMatrix'),
      uSampler: gl.getUniformLocation(program, 'uSampler'),
    },
  };
};
