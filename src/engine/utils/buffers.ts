export const createArrayBuffer = (gl: WebGLRenderingContext, data: number[]): WebGLBuffer => {
  const buffer = gl.createBuffer();

  if (buffer === null) {
    throw new Error('Failed to create buffer');
  }

  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data), gl.STATIC_DRAW);

  return buffer;
};

export const createElementArrayBuffer = (gl: WebGLRenderingContext, data: number[]): WebGLBuffer => {
  const buffer = gl.createBuffer();

  if (buffer === null) {
    throw new Error('Failed to create buffer');
  }

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(data), gl.STATIC_DRAW);

  return buffer;
};

export const enableVec3Float32Buffer = (gl: WebGLRenderingContext, location: number, buffer: WebGLBuffer): void => {
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.vertexAttribPointer(
    location,
    3,
    gl.FLOAT,
    false,
    0,
    0,
  );
  gl.enableVertexAttribArray(location);
};

export const enableVec2Float32Buffer = (gl: WebGLRenderingContext, location: number, buffer: WebGLBuffer): void => {
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.vertexAttribPointer(
    location,
    2,
    gl.FLOAT,
    false,
    0,
    0,
  );
  gl.enableVertexAttribArray(location);
};

export const enableElementArrayBuffer = (gl: WebGLRenderingContext, buffer: WebGLBuffer): void => {
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
};
