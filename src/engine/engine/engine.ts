import { type Model } from '../models';
import { SimpleModel } from '../models/simple';
import { Render } from '../render';

export class Engine {
  private readonly __context: WebGLRenderingContext;

  private readonly __render: Render;

  private readonly __models: Model[];
  private __then: number = 0;

  private __frameCounter = 0;

  constructor (gl: WebGLRenderingContext) {
    this.__context = gl;
    this.__render = new Render(this.__context);

    this.__models = [
      new SimpleModel(this.__context),
    ];
  }

  public async init (): Promise<void> {
    this.__render.init();
    for (const model of this.__models) {
      await model.init();
    }
  }

  public render (now: number): void {
    now *= 0.001;
    const delta = now - this.__then;
    this.__then = now;

    // if (this.__frameCounter % 60 === 0) {
    //   console.log('FPS', (1 / delta).toFixed(0.1));
    // }

    this.__render.draw(this.__models, delta);

    this.__frameCounter++;

    requestAnimationFrame(this.render.bind(this));
  }
}
