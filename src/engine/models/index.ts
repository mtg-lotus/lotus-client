import type { RenderArgs } from '../render';

export abstract class Model {
  protected readonly _context: WebGLRenderingContext;

  protected _positionBuffer: WebGLBuffer | null = null;
  protected _positions: number[] = [];

  public get position (): WebGLBuffer | null {
    return this._positionBuffer;
  }

  constructor (gl: WebGLRenderingContext) {
    this._context = gl;
  }

  public async init (): Promise<void> {
    const buf = this._context.createBuffer();

    if (buf === null) {
      alert('Failed to create buffer');
      return;
    }

    this._positionBuffer = buf;
    this._context.bindBuffer(this._context.ARRAY_BUFFER, this._positionBuffer);

    this._context.bufferData(this._context.ARRAY_BUFFER, new Float32Array(this._positions), this._context.STATIC_DRAW);
  }

  public abstract render ({ projMat, modelViewMat }: RenderArgs, delta: number): void;
}
