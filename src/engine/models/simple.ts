import { type RenderArgs } from '../render';

import { Model } from '.';
import { Shader } from '../shaders';
import { mat4 } from 'gl-matrix';
import { Texture } from '../texture';

import { Mesh } from '../mesh';

export class SimpleModel extends Model {
  private readonly __simpleShader: Shader<'simple'>;
  private readonly __simpleTexture: Texture;
  private __meshes: Mesh[] = [];

  private __rotation: number = 0;

  constructor (gl: WebGLRenderingContext) {
    super(gl);

    this.__simpleShader = new Shader(gl, 'simple');
    this.__simpleTexture = new Texture(gl, 'sampleImage.png');
  }

  public async init (): Promise<void> {
    await super.init();
    this.__simpleShader.link();
    this.__simpleTexture.init();
    this.__meshes = await Mesh.fromObjFileObject(this._context, 'model://cube/cube.obj');
    this.__meshes.forEach((mesh) => {
      mesh.init();
    });
  }

  public render ({ projMat, modelViewMat }: RenderArgs, delta: number): void {
    const {
      attribLocations,
      uniformLocations,
    } = this.__simpleShader.getInfo();

    this.__rotation += delta;

    this._context.useProgram(this.__simpleShader.program);

    mat4.rotate(
      modelViewMat,
      modelViewMat,
      this.__rotation,
      [0, 0, 1],
    );

    mat4.rotate(
      modelViewMat,
      modelViewMat,
      this.__rotation * 0.7,
      [0, 1, 0],
    );

    mat4.rotate(
      modelViewMat,
      modelViewMat,
      this.__rotation * 0.3,
      [1, 0, 0],
    );

    this._context.uniformMatrix4fv(
      uniformLocations.projMat,
      false,
      projMat,
    );

    this._context.uniformMatrix4fv(
      uniformLocations.modelViewMat,
      false,
      modelViewMat,
    );

    this._context.activeTexture(this._context.TEXTURE0);
    this._context.bindTexture(this._context.TEXTURE_2D, this.__simpleTexture.texture);

    this._context.uniform1i(uniformLocations.uSampler, 0);

    this.__meshes.forEach((mesh) => {
      mesh.render({
        vertex: attribLocations.vertexPosition,
        uv: attribLocations.textureCoord,
      });
    });
  }
}
