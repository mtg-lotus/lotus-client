import { app, BrowserWindow, protocol, type App } from 'electron';
import path from 'path';
import { IPC } from './icp';

export interface ElectronWindowConfig {
  width?: number
  height?: number
}

const DEFAULT_CONFIG: ElectronWindowConfig = {
  width: 800,
  height: 800,
};

export class ElectronWindow {
  private __mainWindow: BrowserWindow | null = null;
  private readonly __application: App;
  private readonly __ipc: IPC;

  private readonly __title: string;
  private readonly __config: ElectronWindowConfig;

  constructor (title: string, app: App, config?: ElectronWindowConfig) {
    this.__title = title;

    if (config) {
      this.__config = config;
    } else {
      this.__config = DEFAULT_CONFIG;
    }

    this.__application = app;
    this.__ipc = new IPC(this.__application);
  }

  public main (): void {
    this.__application.on('window-all-closed', this.__onWindowAllClosed.bind(this));
    this.__application.on('ready', this.__onReady.bind(this));
  }

  private __onWindowAllClosed (): void {
    if (process.platform !== 'darwin') {
      this.__application.quit();
    }
  }

  private __onClose (): void {
    this.__mainWindow = null;
  }

  private async __onReady (): Promise<void> {
    this.__mainWindow = new BrowserWindow({
      width: this.__config.width ?? DEFAULT_CONFIG.width,
      height: this.__config.height ?? DEFAULT_CONFIG.height,
      title: this.__title,
      webPreferences: {
        preload: path.join(__dirname, 'preload.js'),
      },
    });

    if (process.env.NODE_ENV === 'development') {
      await this.__mainWindow
        .loadURL('http://localhost:3000/app/');
      this.__mainWindow.webContents.openDevTools();
    } else {
      await this.__mainWindow
        .loadURL(`file://${path.join(__dirname, './index.html')}`);
    }

    this.__mainWindow.on('closed', this.__onClose.bind(this));
  }
}

const win = new ElectronWindow('Lotus', app, {
  width: 1000,
});

win.main();
