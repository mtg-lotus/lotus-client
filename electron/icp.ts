import { ipcMain, type App, type IpcMainEvent } from 'electron';
import Store from 'electron-store';
import { StoreKey } from './electron';

const store = new Store();

export class IPC {
  private readonly __app: App;

  constructor (app: App) {
    this.__app = app;

    // electron-store
    ipcMain.on('electron-store-set-item', this.__setElectronStorageItem.bind(this));
    ipcMain.on('electron-store-get-item', this.__getElectronStorageItem.bind(this));
    ipcMain.on('electron-store-delete-item', this.__deleteElectronStorageItem.bind(this));
  }

  private __setElectronStorageItem (_e: IpcMainEvent, key: StoreKey, val: string): void {
    if (!val) {
      console.log('CANNOT SET', key, val);
      return;
    }
    store.set(key, val);
  }

  private __getElectronStorageItem (e: IpcMainEvent, key: StoreKey): void {
    const item = store.get(key) as (string | undefined);
    e.reply(`returned-item-${key}`, item);
  }

  private __deleteElectronStorageItem (_e: IpcMainEvent, key: StoreKey): void {
    store.delete(key);
  }
}
