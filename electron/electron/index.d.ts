export {};

export type StoreKey = 'tokens.accessToken' | 'tokens.refreshToken';
declare global {
  type ApiEvent = 'nothing-to-download';

  interface Window {
    electronStore: {
      setItem: (key: StoreKey, val: string) => void
      getItem: (key: StoreKey) => Promise<string | undefined>
      deleteItem: (key: StoreKey) => void
    }

    electronApi: {
      downloadAssets: () => void
      on: (eventType: ApiEvent, cb: (...args: any[]) => void) => void
    }
  }
}
