import { contextBridge, ipcRenderer } from 'electron';

const electronStore: Window['electronStore'] = {
  setItem: (key, val) => {
    ipcRenderer.send('electron-store-set-item', key, val);
  },
  getItem: async (key) => {
    return await new Promise((resolve, _reject) => {
      ipcRenderer.once(`returned-item-${key}`, (_e, val: string | undefined) => {
        resolve(val);
      });

      ipcRenderer.send('electron-store-get-item', key);
    });
  },
  deleteItem: (key) => {
    ipcRenderer.send('electron-store-delete-item', key);
  },
};

const electronApi: Window['electronApi'] = {
  downloadAssets: () => {
    ipcRenderer.send('download-assets');
  },
  on: (eventType, cb) => {
    ipcRenderer.on(eventType, cb);
  },
};

contextBridge.exposeInMainWorld('electronStore', electronStore);
contextBridge.exposeInMainWorld('electronApi', electronApi);
